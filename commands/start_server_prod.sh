#!/bin/bash

pip install psycopg2

#python src/manage.py runserver --settings=app.settings."${MODE}" "0:${WSGI_PORT}"
gunicorn -w "${WSGI_WORKERS}" -b 0:"${WSGI_PORT}" --chdir ./src app.wsgi:application --log-level=${WSGI_LOG_LEVEL}