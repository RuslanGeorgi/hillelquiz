from django import forms
from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet, ModelForm, modelformset_factory, BaseFormSet

from quiz.models import Choice


class QuestionsInlineFormSet(BaseInlineFormSet):

    def clean(self):
        if not (self.instance.QUESTION_MIN_LIMIT <= len(self.forms) <= self.instance.QUESTION_MAX_LIMIT):
            raise ValidationError('Quantity of questions is out of range ({}..{})'.format(
                self.instance.QUESTION_MIN_LIMIT, self.instance.QUESTION_MAX_LIMIT
            ))

        question_numbers = sorted([form.cleaned_data['order_number'] for form in self.forms])
        index_set = [i for i in range(1, len(self.forms) + 1)]
        for number, index in zip(question_numbers, index_set):
            if number != index:
                raise ValidationError(f'Wrong question number ({number} is unacceptable)')


class ChoiceInlineFormSet(BaseInlineFormSet):

    def clean(self):
        if not (self.instance.ANSWER_MIN_LIMIT <= len(self.forms) <= self.instance.ANSWER_MAX_LIMIT):
            raise ValidationError('Quantity of answers is out of range ({}..{})'.format(
                self.instance.ANSWER_MIN_LIMIT, self.instance.ANSWER_MAX_LIMIT
            ))

        total_number = sum(
            1
            for form in self.forms
            if form.cleaned_data['is_correct']
        )

        if total_number == len(self.forms):
            raise ValidationError('NOT allowed to select all choices')

        if total_number == 0:
            raise ValidationError('AT LEAST 1 choice should be selected')


class ChoiceForm(ModelForm):
    is_selected = forms.BooleanField(required=False)

    class Meta:
        model = Choice
        fields = ['text']

# class RequiredFormSet(BaseInlineFormSet):
#     def clean(self):
#         if any(self.errors):
#             return
#         if not self.forms[0].has_changed():
#             raise ValidationError('Please add at least one vehicle.')


ChoiceFormSet = modelformset_factory(
    model=Choice,
    form=ChoiceForm,
    extra=0
)
