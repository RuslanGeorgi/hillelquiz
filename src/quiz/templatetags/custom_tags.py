from django import template

register = template.Library()


def negate(value):
    return -value


def division(numerator, denominator):
    return numerator / denominator


def multiplication(mult1, mult2):
    return mult1 * mult2


def expr(value, *args):
    for idx, arg in enumerate(args, 1):
        value = value.replace(f'%{idx}', str(arg))
    return eval(value)


register.filter('negate', negate)
register.filter('div', division)
register.filter('mult', multiplication)
register.simple_tag(name='expr', func=expr)
