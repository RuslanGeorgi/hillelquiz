from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.views.generic.list import MultipleObjectMixin

from core.utils import get_selection_error
from quiz.forms import ChoiceFormSet
from quiz.models import Test, Result, Question


class TestListView(LoginRequiredMixin, ListView):
    model = Test
    template_name = 'tests/list.html'
    context_object_name = 'tests'
    login_url = reverse_lazy('accounts:login')


class TestDetailView(LoginRequiredMixin, DetailView, MultipleObjectMixin):
    model = Test
    template_name = 'tests/details.html'
    context_object_name = 'test'
    pk_url_kwarg = 'uuid'
    paginate_by = 5
    login_url = reverse_lazy('accounts:login')

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.model.objects.get(uuid=uuid)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(object_list=self.get_queryset(), **kwargs)
        # context['result_list'] = self.get_queryset()
        return context

    def get_queryset(self):
        return Result.objects.filter(
            test=self.get_object(),
            user=self.request.user
        ).order_by('state')


class TestResultDetailsView(LoginRequiredMixin, DetailView):
    model = Result
    template_name = 'results/details.html'
    context_object_name = 'result'
    pk_url_kwarg = 'uuid'
    login_url = reverse_lazy('accounts:login')

    def get_object(self):
        uuid = self.kwargs.get('result_uuid')
        return self.get_queryset().get(uuid=uuid)


class TestResultCreateView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('accounts:login')

    def post(self, request, uuid):

        result = Result.objects.create(
            user=request.user,
            test=Test.objects.get(uuid=uuid),
            state=Result.STATE.NEW,
            current_order_number=0
        )
        result.save()

        return HttpResponseRedirect(reverse(
            'tests:question',
            kwargs={
                'uuid': uuid,
                'result_uuid': result.uuid,
                # 'order_number': 1
            }
        ))


class TestResultUpdateView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('accounts:login')

    def get(self, request, uuid, result_uuid):
        # result = Result.objects.get(uuid=result_uuid)

        return HttpResponseRedirect(reverse(
            'tests:question',
            kwargs={
                'uuid': uuid,
                'result_uuid': result_uuid,
                # 'order_number': result.current_order_number + 1
            }
        ))


class TestResultQuestionView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('accounts:login')

    def get(self, request, uuid, result_uuid):

        order_number = Result.objects.get(uuid=result_uuid).current_order_number + 1
        question = Question.objects.get(
            order_number=order_number,
            test__uuid=uuid  # same as Test.objects.get(uuid=uuid)
        )

        choices = ChoiceFormSet(queryset=question.choices.all())

        return render(
            request=request,
            template_name='tests/question.html',
            context={
                'question': question,
                'choices': choices
            }
        )

    def post(self, request, uuid, result_uuid):
        order_number = Result.objects.get(uuid=result_uuid).current_order_number + 1
        question = Question.objects.get(
            order_number=order_number,
            test__uuid=uuid  # same as Test.objects.get(uuid=uuid)
        )

        choices = ChoiceFormSet(data=request.POST)
        selected_choices = [
            'is_selected' in form.changed_data
            for form in choices.forms
        ]

        if get_selection_error(selected_choices):
            messages.warning(self.request, get_selection_error(selected_choices))
            return HttpResponseRedirect(reverse(
                'tests:question',
                kwargs={
                    'uuid': uuid,
                    'result_uuid': result_uuid
                }
            ))

        result = Result.objects.get(
            uuid=result_uuid
        )
        result.update_result(order_number, question, selected_choices)

        if result.state == Result.STATE.FINISHED:
            return HttpResponseRedirect(reverse(
                'tests:result_details',
                kwargs={
                    'uuid': uuid,
                    'result_uuid': result.uuid
                }
            ))
        else:
            return HttpResponseRedirect(reverse(
                'tests:question',
                kwargs={
                    'uuid': uuid,
                    'result_uuid': result.uuid,
                    # 'order_number': order_number + 1
                }
            ))


class TestResultDeleteView(LoginRequiredMixin, DeleteView):
    model = Result
    template_name = 'results/delete.html'
    pk_url_kwarg = 'uuid'
    success_url = reverse_lazy('tests:details')
    login_url = reverse_lazy('accounts:login')

    def get_object(self):
        uuid = self.kwargs.get('result_uuid')
        return self.get_queryset().get(uuid=uuid)

    def get_success_url(self):
        test_uuid = self.kwargs.get('uuid')
        return reverse_lazy('tests:details', kwargs={'uuid': test_uuid})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['test_uuid'] = self.kwargs.get('uuid')
        return context
