from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, UpdateView, ListView

from accounts.forms import AccountRegistrationForm, AccountUpdateForm
from accounts.models import User
from quiz.models import Result, Test


class AccountRegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f'User {self.request.POST["username"]} has successfully created')
        return result


class AccountLoginView(LoginView):
    success_url = reverse_lazy('index')
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('index')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f'User {self.request.user} has successfully logged in')
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def dispatch(self, request, *args, **kwargs):
        messages.success(self.request, f'User {self.request.user} has logged out')
        return super().dispatch(request, *args, **kwargs)


class AccountUpdateView(UpdateView):
    model = User
    template_name = 'profile.html'
    success_url = reverse_lazy('index')
    form_class = AccountUpdateForm

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f'Account of user {self.request.user} has been successfully updated')
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(object_list=self.get_queryset(), **kwargs)
        # context['result_list'] = self.get_queryset()
        return context

    def get_queryset(self):
        return self.object.results.all().order_by('state')


class AccountPwdChangeView(PasswordChangeView):
    model = User
    template_name = 'password.html'
    success_url = reverse_lazy('accounts:profile')

    def get_object(self):
        return self.request.user


class LeaderboardView(LoginRequiredMixin, ListView):
    model = User
    template_name = 'leaderboard.html'
    context_object_name = 'users'
    queryset = User.objects.filter(rating__gt=0).order_by('-rating')
