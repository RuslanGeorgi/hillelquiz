from accounts.views import AccountLoginView, AccountLogoutView, AccountPwdChangeView, \
    AccountRegistrationView, AccountUpdateView, LeaderboardView

from django.urls import path

app_name = 'accounts'

urlpatterns = [
    path('registration/', AccountRegistrationView.as_view(), name='registration'),
    path('login/', AccountLoginView.as_view(), name='login'),
    path('logout/', AccountLogoutView.as_view(), name='logout'),
    path('profile/', AccountUpdateView.as_view(), name='profile'),
    path('password/', AccountPwdChangeView.as_view(), name='password'),
    path('leaderboard/', LeaderboardView.as_view(), name='rating'),
]
