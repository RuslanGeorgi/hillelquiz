from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from accounts.models import User


class AccountRegistrationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class AccountUpdateForm(UserChangeForm):

    class Meta(UserChangeForm.Meta):
        model = User
        fields = [
            'username', 'first_name', 'last_name', 'email',
            'bio', 'location', 'birth_date', 'image'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        password = self.fields.get('password')
        if password:
            password.help_text = password.help_text.replace(
                '../password/', './password')
