import uuid


def generate_uuid():
    result = uuid.uuid4()  # .hex
    return result


def get_selection_error(selected_choices):
    message = None
    if any(selected_choices) is False:
        message = 'AT LEAST 1 choice should be selected'

    if all(selected_choices) is True:
        message = 'NOT allowed to select all choices'

    return message
